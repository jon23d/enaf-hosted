export const default_state = {
	interface: {
		module: 'account',
		active_account: 'Credit Card'
	},
	Accounts: {
		'Checking': {
			is_income_account: true,
			balance: 100.00,
			transactions: [
				{
					id: 1,
					date: '2016-05-30',
					payee: 'Trunks bar',
					category: 'alcohol',
					debit: 43.00,
					credit: 0.00,
					memo: 'A bit of drinking',
					is_cleared: true
				},
				{
					id: 2,
					date: '2016-05-30',
					payee: 'La Gringa',
					category: 'restaurants',
					debit: 7.92,
					credit: 0.00,
					memo: 'A burrito for Jorge',
					is_cleared: false
				}
			]
		},
		'Credit Card': {
			is_income_account: false,
			balance: -100.00,
			transactions: [
				{
					id: 1,
					date: '2016-05-15',
					payee: 'Amazon',
					category: 'household goods',
					debit: 172.34,
					credit: 0.00,
					memo: 'Juicemax 10,000,000',
					is_cleared: false
				},
				{
					id: 2,
					date: '2016-05-11',
					payee: 'Joe\'s place',
					category: 'restaurants',
					debit: 22.47,
					credit: 0.00,
					memo: 'Dinner alone',
					is_cleared: false
				}
			]
		}
	}
};
