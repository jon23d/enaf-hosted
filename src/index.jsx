import React from 'react';
import ReactDOM from 'react-dom';
import {createStore} from 'redux';
import {Provider} from 'react-redux';
import {Router, Route, browserHistory} from 'react-router';

import reducer from './reducer';
import {default_state} from './default_state';

import {AppContainer} from './components/App';
import Dashboard from './components/Dashboard';
import {TransactionsListContainer} from './components/TransactionsList';

const store = createStore(
	reducer,
	default_state,
	// This is for the redux dev tools chrome extension
	window.devToolsExtension && window.devToolsExtension()
);

const routes = <Route component={AppContainer}>
	<Route path="/" component={Dashboard} />
	<Route path="/Account/:account_name" component={TransactionsListContainer} />
</Route>;

ReactDOM.render(
	<Provider store={store}>
		<Router history={browserHistory}>{routes}</Router>
	</Provider>,
	document.getElementById('app')
);