import React from 'react';
import {connect} from 'react-redux';
import {Link} from 'react-router';

export const App = React.createClass({
	getAccounts: function() {
		return this.props.Accounts || {};
	},

	render: function() {
		return <div>
			<div id="left-menu">
				<div id="budget-menu">
					<button>Budget</button>
					<button>All Accounts</button>
				</div>
				<div id="accounts-menu">
					<em>All Accounts</em>
					{Object.keys(this.getAccounts()).map(account =>
						<Link to={`/Account/${account}`} key={account}><button>{account}</button></Link>
					)}
				</div>
				<button>Add account</button>
			</div>
			<div id="container">
				{this.props.children}
			</div>
		</div>
	}
});

function mapStateToProps(state) {
	return {
		Accounts: state.Accounts
	}
}

export const AppContainer = connect(mapStateToProps)(App);