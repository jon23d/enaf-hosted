import React from 'react';
import {connect} from 'react-redux';

export const NewTransactionRow = React.createClass({
	addRow: function() {
        this.props.dispatch({
            type: 'ADD_TRANSACTION',
            date: this.refs.date.value,
            category: this.refs.category.value,
            payee: this.refs.payee.value,
            memo: this.refs.memo.value,
            debit: this.refs.debit.value,
            credit: this.refs.credit.value,
            is_cleared: this.refs.is_cleared.value,
            account_name: this.props.account_name
        });
    },

	render: function() {
		return <tr className="new-transaction">
			<td><input type="text" ref="date" /></td>
			<td><input type="text" ref="payee"/></td>
			<td><input type="text" ref="category"/></td>
			<td><input type="text" ref="memo"/></td>
			<td><input type="text" ref="debit"/></td>
			<td><input type="text" ref="credit"/></td>
			<td>
				<input type="text" ref="is_cleared"/><br />
				<button onClick={this.addRow}>Add</button>
			</td>
		</tr>;
	}
});

function mapStateToProps(state, ownProps) {
	return {
        account_name: ownProps.account_name
    };
}

function mapDispatchToProps(dispatch) {
	
}

export const NewTransactionRowContainer = connect(mapStateToProps)(NewTransactionRow);