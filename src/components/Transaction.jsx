import React from 'react';

export default React.createClass({
	getTransaction: function() {
		return this.props.transaction || {
			date: '',
			payee: '',
			category: '',
			memo: '',
			debit: 0.00,
			credit: 0.00,
			description: '',
			is_cleared: false
		};
	},

	render: function() {
		return <tr className={this.props.even_odd}>
			<td>{this.getTransaction().date}</td>
			<td>{this.getTransaction().payee}</td>
			<td>{this.getTransaction().category}</td>
			<td>{this.getTransaction().memo}</td>
			<td>{this.getTransaction().debit.toFixed(2)}</td>
			<td>{this.getTransaction().credit.toFixed(2)}</td>
			<td>{(this.getTransaction().is_cleared) ? 'yes' : 'no'}</td>
		</tr>
	}
});