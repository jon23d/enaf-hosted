import React from 'react';
import {connect} from 'react-redux';
import {fromJS} from 'immutable';

import {NewTransactionRowContainer} from 'NewTransactionRow';
import Transaction from 'Transaction';

require('../css/style.css');

export const TransactionsList = React.createClass({
	render: function() {
		var even_odd = 0;

		return <table id="transactions">
				<thead>
					<tr>
						<th>Date</th>
						<th>Payee</th>
						<th>Category</th>
						<th>Memo</th>
						<th>Debit</th>
						<th>Credit</th>
						<th>Cleared</th>
					</tr>
				</thead>
				<tbody>
					<NewTransactionRowContainer account_name={this.props.account_name} />
					{this.props.transactions.map(transaction =>
						<Transaction
							transaction={transaction}
							key={transaction.id}
							even_odd={(++even_odd % 2 == 0) ? 'even' : 'odd'}
						/>
					)}
				</tbody>
		</table>
	}
});

function mapStateToProps(state, ownProps) {
	return {
		account_name: ownProps.params.account_name,
		transactions: fromJS(state).getIn(['Accounts',ownProps.params.account_name, 'transactions']).toJS()
	}
}

export const TransactionsListContainer = connect(mapStateToProps)(TransactionsList);