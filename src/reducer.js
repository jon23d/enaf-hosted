import {default_state} from './default_state';
import {fromJS} from 'immutable';

export default function(state = default_state, action) {
	switch (action.type) {
		case 'ADD_TRANSACTION':
			const path = ['Accounts',action.account_name, 'transactions'];
			const transactions = fromJS(state).getIn(path);

			return fromJS(state).setIn(path, transactions.unshift({
				id: 3,
				date: action.date,
				category: action.category,
				memo: action.memo,
				debit: parseFloat(action.debit),
				credit: parseFloat(action.credit),
                payee: action.payee,
				is_cleared: false
			})).toJS();
	}
	return state;
}